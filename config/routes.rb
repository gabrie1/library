Rails.application.routes.draw do
  devise_for :librarians
  root 'books#index'
  resources :reservations
  resources :books
  resources :authors
  resources :categories
  resources :librarians
  resources :clients
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
