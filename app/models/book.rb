class Book < ApplicationRecord  
  belongs_to :categorie
  belongs_to :author
  has_many :reservations, dependent: :destroy

  validates :name, :author, :categorie, :stock, presence: true
  validates :stock, numericality: { only_integer: true, greater_than_or_equal_to: 0}
  
end
