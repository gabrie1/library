class Reservation < ApplicationRecord
  validates :book, :librarian, :client, presence: true

  belongs_to :book
  belongs_to :client
  belongs_to :librarian

  after_create :decrease_the_stock
  after_destroy :increase_the_stock


  private def increase_the_stock
    self.book.stock = self.book.stock + 1
    self.book.save
  end
  
  private def decrease_the_stock
    self.book.stock = self.book.stock - 1
    self.book.save
  end

end
