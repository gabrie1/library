class Librarian < ApplicationRecord
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable, :trackable, :omniauthable, :validatable, :registerable
    devise :database_authenticatable,
        :recoverable, :rememberable

    validates :name, :email, :password, presence:true
    validates :password, confirmation: true
    validates :password, length: { minimum: 6 }
    validates :email, uniqueness: true
    validates_format_of :email, :with =>  /\A[^@,\s]+@[^@,\s]+\.[^@,\s]+\z/

    has_many :reservations, dependent: :destroy


            
end
