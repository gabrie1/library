class ApplicationController < ActionController::Base
  layout 'admin_lte_2'

  before_action :authenticate_librarian!
end
