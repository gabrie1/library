# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Generating the Librarians"
    5.times do |i|
        Librarian.create!(
            name: Faker::Name.name,
            email: Faker::Internet.email,
            password: "123456"
        )
    end    
puts"OK"

puts "Generating the Categories"
    5.times do |i|
        Categorie.create!(
            name: Faker::Book.genre
        )
    end
puts "OK"

puts "Generating the Authors"
    5.times do |i|
        Author.create!(
            name: Faker::Book.author
        )
    end
puts "OK"

puts "Generating the Clients"
    5.times do |i|
        Client.create!(
            name: Faker::Name.name
        )
    end    
puts"OK"

puts "Generating the Books"
    vetor = [10,11,12,13,14,15,16,17,18,19,21]
    20.times do |i|
        Book.create!(
            name: Faker::Book.title,
            author: Author.all.sample(),
            categorie: Categorie.all.sample(),
            stock: vetor.sample()
        )
    end    
puts"OK"

puts "Generating the Reservations"
    10.times do |i|
        k = Reservation.create!(
            book: Book.all.sample(),
            client: Client.all.sample(),
            librarian: Librarian.all.sample()
        )
    end    
puts"OK"


