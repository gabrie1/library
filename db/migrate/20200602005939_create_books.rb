class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.integer :stock
      t.string :name
      t.references :categorie, foreign_key: true
      t.references :author, foreign_key: true

      t.timestamps
    end
  end
end
